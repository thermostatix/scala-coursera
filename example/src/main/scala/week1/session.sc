object session {

  def sqrt(x: Double): Double = {
    def sqrIter(guess: Double): Double =
      if (isGoodEnough(guess)) guess
      else
        sqrIter(improve(guess))

    def isGoodEnough(guess: Double): Boolean =
      abs(guess * guess - x) < x * 0.001

    def improve(guess: Double): Double =
      (guess + x / guess) / 2 //calculate the mean of the guesstimate and x / guess

    def abs(d: Double) = if (d < 0) -d else d

    sqrIter(1.0)
  }

  sqrt(4)
  sqrt(2)
  sqrt(1e-6)

  def factorial(n: Int): Int = {
    def loop(acc: Int, n: Int): Int = {
      if (n == 0) acc
      else acc + loop(acc * n, n-1)
    }
    loop(1, n)
  }


  factorial(4)
}

