object session {
  def outerCount(money: Int, coins: List[Int]): Int = coins match {
    case Nil => 0
    case x :: Nil => count(money, x)
    case _ => count(money, coins.head) + outerCount(money, coins.tail) + outerCount(money - coins.head, coins.tail)*(money / coins.head)
  }

  def count(change: Int, coin: Int): Int = {
    if (change - coin < 0) 0 else {
      if (change - coin == 0) 1 else
        count(change - coin, coin)
    }
  }

  outerCount(6, List(1,2,3).reverse)

  count(6,3)

  outerCount(6, List(2,1))
}