object session {
  //experiment with new data structure: class.
  val x = new Rational(1, 2)
  x.numer
  x.denom
  val y = new Rational(2, 3)
  val z = new Rational(17, 21)


  println("x + y")
  x.add(y)


  println("x-y-z")
  x.sub(y).sub(z)


  println("max(x,y)")
  x.max(y)


  println("is x less that y?")
  x.less(y)


  print("find the maximum between " + x + " and " + y + ": ")
  x.max(y)


  //val strange = new Rational(1,0)
  //strange.add(strange)

  println("implement the secondary constructor")
  new Rational(2)

  // Class Definition of rationals
  class Rational(x: Int, y: Int) {
    require(x!=0, "Division by zero? How DARE you!?!?!")

    //secondary constructor
    def this(x: Int) = this(x,1)

    private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
    private val g = gcd(x, y)
    def numer = x / g
    def denom = y / g

    def add(that: Rational) =
      new Rational(
        numer * that.denom + that.numer * denom,
        denom * that.denom)

    def neg: Rational = new Rational(numer * -1, denom)

    def sub(that: Rational) = add(that.neg)

    def less(that: Rational) = numer * that.denom < that.numer * denom

    def max(that: Rational) = if (this.less(that)) that else this

    override def toString: String = numer + "/" + denom
  }

}


