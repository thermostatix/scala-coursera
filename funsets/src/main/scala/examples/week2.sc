object session {
  //Week 2-1: Higher order functions
  //optimised sum function (tail recursive version)
  def sum(f: Int => Int, a: Int, b: Int): Int = {
    def loop(a: Int, acc: Int): Int =
      if (a > b) acc else loop(a + 1, f(a) + acc)

    loop(a, 0)
  }

  //sum of integers
  sum(x => x, 1, 3)
  //sum of squares
  sum(x => x * x, 3, 5)

}

