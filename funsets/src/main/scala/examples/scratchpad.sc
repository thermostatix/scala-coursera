type Set = Int => Boolean

def contains(s: Set, elem: Int): Boolean = s(elem)

def singletonSet(elem: Int): Set = {
  (x: Int) => x == elem
}

val s1 = singletonSet(1)

contains(s1, 22)

contains(s1, 1)

/**
  * Returns the subset of `s` for which `p` holds.
  */
def filter(s: Set, p: Int => Boolean): Set = (x: Int) => {
  p(x)
}

filter(s1, 1 => 1<0)