object week2_2_mapReduce{

  // Week 2-2: Currying
  //curry sum function syntax
  def sum(f: Int => Int, a: Int, b: Int): Int = {
    if (a > b) 0 else f(a) + sum(f, a + 1, b)
  }

  //Curried sum of ints
  sum(x => x, 1, 3)

  //curried version of product
  def product(f: Int => Int)(a: Int, b: Int): Int = {
    if (a > b) 1 else f(a) * product(f)(a + 1, b)
  }

  //Product of integers
  product(x => x)(1, 3)

  //factorial in terms of product:
  def fact(n: Int) = product(x => x)(1, n)

  //Factorial:
  fact(3)
  fact(10)

  //mapReduce example: write a function that generalizes product and sum
  def mapReduce(f: Int => Int, combine: (Int, Int) => Int, identity: Int)(a: Int, b: Int): Int = {
    if (a > b) identity
    else combine(f(a), mapReduce(f, combine, identity)(a + 1, b))
  }

  //mapReduce version of product
  def prod(f: Int => Int)(a: Int, b: Int): Int = mapReduce(f, (x, y) => x * y, 1)(a, b)

  //as before, this is the product of ints:
  prod(x => x)(1, 4)

  //...and the product of cubes:
  prod(x => x * x * x)(1, 4)

  //as before, defining factorial in terms of product
  def factorial(n: Int) = product(x => x)(1, n)

  factorial(3)

  /* NOTE:
    Lecturer had some issues when first trying to run the mapReduce product. The order
    of the function definitions becomes important when defining and using a function in another
    function.
    TODO: Better explanation required
   */
}
