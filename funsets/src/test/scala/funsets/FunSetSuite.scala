package funsets

import org.scalatest.FunSuite


import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

/**
  * This class is a test suite for the methods in object FunSets. To run
  * the test suite, you can either:
  *  - run the "test" command in the SBT console
  *  - right-click the file in eclipse and chose "Run As" - "JUnit Test"
  */
@RunWith(classOf[JUnitRunner])
class FunSetSuite extends FunSuite {

  /**
    * Link to the scaladoc - very clear and detailed tutorial of FunSuite
    *
    * http://doc.scalatest.org/1.9.1/index.html#org.scalatest.FunSuite
    *
    * Operators
    *  - test
    *  - ignore
    *  - pending
    */

  /**
    * Tests are written using the "test" operator and the "assert" method.
    */
  // test("string take") {
  //   val message = "hello, world"
  //   assert(message.take(5) == "hello")
  // }

  /**
    * For ScalaTest tests, there exists a special equality operator "===" that
    * can be used inside "assert". If the assertion fails, the two values will
    * be printed in the error message. Otherwise, when using "==", the test
    * error message will only say "assertion failed", without showing the values.
    *
    * Try it out! Change the values so that the assertion fails, and look at the
    * error message.
    */
  // test("adding ints") {
  //   assert(1 + 2 === 3)
  // }


  import FunSets._

  test("contains is implemented") {
    assert(contains(x => true, 100))
  }

  /**
    * When writing tests, one would often like to re-use certain values for multiple
    * tests. For instance, we would like to create an Int-set and have multiple test
    * about it.
    *
    * Instead of copy-pasting the code for creating the set into every test, we can
    * store it in the test class using a val:
    *
    * val s1 = singletonSet(1)
    *
    * However, what happens if the method "singletonSet" has a bug and crashes? Then
    * the test methods are not even executed, because creating an instance of the
    * test class fails!
    *
    * Therefore, we put the shared values into a separate trait (traits are like
    * abstract classes), and create an instance inside each test method.
    *
    */

  trait TestSets {
    val s0 = singletonSet(0)
    val s1 = singletonSet(1)
    val s2 = singletonSet(2)
    val s3 = singletonSet(3)
    val s4 = singletonSet(4)
    val s5 = singletonSet(5)
    val s6 = singletonSet(6)
  }

  trait TestEvenSet {
  }

  /**
    * This test is currently disabled (by using "ignore") because the method
    * "singletonSet" is not yet implemented and the test would fail.
    *
    * Once you finish your implementation of "singletonSet", exchange the
    * function "ignore" by "test".
    */
  test("singletonSet(1) contains 1, singletonSet(2) does not contain 3") {

    /**
      * We create a new instance of the "TestSets" trait, this gives us access
      * to the values "s1" to "s3".
      */
    new TestSets {
      /**
        * The string argument of "assert" is a message that is printed in case
        * the test fails. This helps identifying which assertion failed.
        */
      assert(contains(s1, 1), "Singleton")
      assert(!contains(s2, 3), "Singleton set 2 does not contain 3")
      assert(!contains(s3, 4), "s3 does not contain 4")
    }
  }

  test("union contains all elements of each set") {
    new TestSets {
      val s = union(s1, s2)
      assert(contains(s, 1), "Union 1")
      assert(contains(s, 2), "Union 2")
      assert(!contains(s, 3), "Union 3")
    }
  }

  test("diff excludes the second variable") {
    /**
      * These tests were written to ensure the code works, ie after the fact. However, the penny dropped once union was
      * figured out, so this was merely to test own understanding and get used to writing functions as variables
      */

    new TestSets {
      val d = diff(s1, s2)
      assert(contains(d, 1) && contains(s1, 1) && !contains(s2, 1), "Difference between s1 and s2 is that 1 is only in s1")
    }
  }

  test("intersection and diff requires a few unions ") {
    new TestSets {
      val u = union(s1, s3) // u E {1, 3}
      val u2 = union(u, s2) // u2 E {u, 2} >> u2 E {1, 2, 3}
      /**
        * validate above unions first
        */
      assert(contains(u, 1) && contains(u, 3) && !contains(u, 2), "u E {1, 3}")
      assert(contains(u2, 1) && contains(u2, 2) && contains(u, 3), "u E {1, 2, 3}")

      /**
        * now test the intersection of a few sets
        */
      val i1 = intersect(s1, s2) // i1 E {}
      val i2 = intersect(s1, u2) // i2 E {1}
      val i3 = intersect(u2, s3) // i3 E {3}
      assert(!(contains(i1, 1) || contains(i1, 2) || contains(i1, 3)), "i1 E {} >> should be empty")
      /**
        * diff a few
        */
      val d1 = diff(u2, u) // d1 E {2}
      val d2 = diff(singletonSet(4), u2) // d2 E {4}
      assert(contains(d1, 2), "d1 should contain 2")
      assert(!contains(d1, 1), "d1 should NOT contain 1")
      assert(contains(d2, 4), "d2 should contain only 4")

    }
  }

  test("filter _ < 5") {
    new TestSets {
      val u = union(s1, union(s2, s4))
      val u6 = union(u, s6)
      assert(contains(diff(u6, u), 6), "Check that u6 contains 6 and u does not")
      val lessThanFive = (x: Int) => x < 5
      assert(lessThanFive(4), "first just check if our test works")
      assert(!lessThanFive(6), "should not be true for 6")
      assert(lessThanFive(-1), "and finally negative numbers")
      //Now for the real test: filter(u)
      val filteredLessThanFive_U6 = filter(u6, x => x < 5)
      assert(contains(diff(u6, filter(u, lessThanFive)), 6), "test if the difference between the filtered set u and the u6 contains 6 and not 4")
    }
  }

  test("forall {1,2,3,4} < 5") {
    new TestSets {
      val u = union(s1, union(s2, union(s3, s4)))
      for (i <- 1 to 4) assert(contains(u, i), "%s should be contained in set u".format(i))
      assert(forall(u, x => x < 5), "less than five!")
    }
  }

  test("even numbers pre-generated") {
    new TestSets {
      val u = (x: Int) => x % 9 == 0
      assert(forall(u, x => x % 3 == 0), "Div by 3 and div by 9!")
    }
  }

  test("test if a number is divisible by 3 in the set {-4,-2,2,4,6}") {
    new TestSets {
      val u = union(s2, union(s4, union(s6, union(singletonSet(-2), singletonSet(-4)))))
      assert(exists(u, x => x % 3 == 0), "divisible by 3, anyone?")
      printSet(u)
      printSet(map(u, x => x * 5))
      assert(forall(map(u, x=> x*5), x=> x%5==0), "test if they are all divisible by 5")
    }
  }

}
