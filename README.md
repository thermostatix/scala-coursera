# README #

This Repository is for keeping tabs on my progress in the Scala Functional Language courses being undertaken on Coursera.

### What is this repository for? ###

This is the first of five courses in the Coursera Specialization, called [Functional Programming Principles in Scala](https://www.coursera.org/learn/progfun1/home/welcome).
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I review what's going on here? ###

For each week there is an assignment. The scaffolding is provided as a `.zip` file which is extracted to a directory under the root directory. 
A separate `*.md` file will be stored under the root directory, with a filename pattern `^assignment[0...9]-<dirname>` where `<dirname>` is the directory name taken from the extracted archive. Non-assignments will most likely be named `exercise-<dirname>` but will remain ad-hoc.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact