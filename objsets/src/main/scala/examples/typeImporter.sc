import week3._

object nth {
  def nth[T](n: Int, xs: List[T]): T =
    if (xs.isEmpty) throw new IndexOutOfBoundsException("WTF, no element at index " + n)
    else if (n == 0) xs.head
    else nth(n-1, xs.tail)

  val longlist = new Cons(10, new Cons(20, new Cons(30, new Nil)))

  nth(2, longlist)
  nth(1, longlist)

  List(1,2).tail
  List(1)
  List()
  List().isEmpty
}

//def singleton[T](elem: T) = new Cons[T](elem, new Nil[T])

//val x = singleton(1)


