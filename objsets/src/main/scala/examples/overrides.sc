object overrides {

  val subval = new Sub()

}

abstract class Base {

  //implementation
  def foo = 1

  //only definition, no implementation >> can only happen inside an abstract class
  def bar: Int

  override def toString: String = "foo = " + foo + "; bar = " + bar
}

class Sub extends Base {

  //have to override because there already exists an implementation in the base class
  override def foo = 2

  //can only implement since it has no implementation
  def bar = 3
}