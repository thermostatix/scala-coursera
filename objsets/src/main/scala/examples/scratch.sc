import week3._

//could also have imported by using
// import week3.Rational


object scratch {
  new Rational(1,3)

  def err(msg: String) = throw new Error(msg)

  //err("WTF")

  val x = null

  val y: String = x


  if (true) 1 else false

}
