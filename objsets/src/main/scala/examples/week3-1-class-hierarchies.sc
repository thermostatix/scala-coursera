import week3._

object session {

  val t1 = new NonEmpty(1, new Empty, new Empty)
  val t4 = new NonEmpty(4, new Empty, new Empty)

  //val t3 = t4 union (t1 incl 5)
  val t3 = t4.incl(8)
  val t5 = t3.union(t1)
  val t14 = t5.union(t4)

  t14.filter(x => x > 2)

  /**
    * Method to create a list in ascending order from the BST values, using Cons
    */
}


