package objsets

import TweetReader._

/**
  * A class to represent tweets.
  */
class Tweet(val user: String, val text: String, val retweets: Int) {
  override def toString: String =
    "User: " + user + "\n" +
      "Text: " + text + " [" + retweets + "]"
}

/**
  * This represents a set of objects of type `Tweet` in the form of a binary search
  * tree. Every branch in the tree has two children (two `TweetSet`s). There is an
  * invariant which always holds: for every branch `b`, all elements in the left
  * subtree are smaller than the tweet at `b`. The elements in the right subtree are
  * larger.
  *
  * Note that the above structure requires us to be able to compare two tweets (we
  * need to be able to say which of two tweets is larger, or if they are equal). In
  * this implementation, the equality / order of tweets is based on the tweet's text
  * (see `def incl`). Hence, a `TweetSet` could not contain two tweets with the same
  * text from different users.
  *
  *
  * The advantage of representing sets as binary search trees is that the elements
  * of the set can be found quickly. If you want to learn more you can take a look
  * at the Wikipedia page [1], but this is not necessary in order to solve this
  * assignment.
  *
  * [1] http://en.wikipedia.org/wiki/Binary_search_tree
  */
abstract class TweetSet {

  def isEmpty: Boolean

  /**
    * This method takes a predicate and returns a subset of all the elements
    * in the original set for which the predicate is true.
    *
    * Question: Can we implement this method here, or should it remain abstract
    * and be implemented in the subclasses?
    *
    * Answer: This method can be implemented here because it is the same in both
    * subclasses, if filterAcc returns acc for the empty set
    *
    */
  def filter(p: Tweet => Boolean): TweetSet = filterAcc(p, new Empty)

  /**
    * This is a helper method for `filter` that propagates the accumulated tweets.
    */
  def filterAcc(p: Tweet => Boolean, acc: TweetSet): TweetSet

  /**
    * Returns a new `TweetSet` that is the union of `TweetSet`s `this` and `that`.
    *
    * Question: Should we implement this method here, or should it remain abstract
    * and be implemented in the subclasses?
    *
    * Answer: Keep abstract. The implementation differs between the two subclasses.
    */
  def union(that: TweetSet): TweetSet

  /**
    * Returns the tweet from this set which has the greatest retweet count.
    *
    * Calling `mostRetweeted` on an empty set should throw an exception of
    * type `java.util.NoSuchElementException`.
    *
    * Question: Should we implment this method here, or should it remain abstract
    * and be implemented in the subclasses?
    *
    * Answer: My initial opinion was Subclasses, as this method's implementation should differ
    * between the two subclasses, and therefore should remain abstract. However, the
    * teaching staff tip on Coursera suggested the use of an accumulator helper function
    * much like in the case of filtering. So I'll be trying out that approach first instead.
    *
    */
  def childrenAreEmpty: Boolean

  def mostRetweeted: Tweet = if (isEmpty) throw new NoSuchElementException else mostRetweetedAcc(new Tweet("", "", 0))

  /**
    * Returns a TweetSet accumulator. Same approach as filterAcc, this function
    * will be used to propagate the maximum retweet value. The search for the
    * lexicographically larger tweet with a corresponding retweets value will
    * be done in mostRetweeted
    */
  def mostRetweetedAcc(maxRTAcc: Tweet): Tweet

  /**
    * Returns a list containing all tweets of this set, sorted by retweet count
    * in descending order. In other words, the head of the resulting list should
    * have the highest retweet count.
    *
    * Hint: the method `remove` on TweetSet will be very useful.
    * Question: Should we implement this method here, or should it remain abstract
    * and be implemented in the subclasses?
    *
    * Answer: I had trouble accessing members of the NonEmpty class from the base class, and
    * I may be wrong but I believe this is due to the fact that the interpreter cannot predict
    * which instance will be calling the methods, from the base class. Therefore I will rather keep this
    * method abstract and simply return Nil from the Empty subclass, even though
    */
  def descendingByRetweet: TweetList

  /**
    * The following methods are already implemented
    */

  /**
    * Returns a new `TweetSet` which contains all elements of this set, and the
    * the new element `tweet` in case it does not already exist in this set.
    *
    * If `this.contains(tweet)`, the current set is returned.
    */
  def incl(tweet: Tweet): TweetSet

  /**
    * Returns a new `TweetSet` which excludes `tweet`.
    */
  def remove(tweet: Tweet): TweetSet

  /**
    * Tests if `tweet` exists in this `TweetSet`.
    */
  def contains(tweet: Tweet): Boolean

  /**
    * This method takes a function and applies it to every element in the set.
    */
  def foreach(f: Tweet => Unit): Unit
}

class Empty extends TweetSet {

  override def childrenAreEmpty: Boolean = true

  /**
    * This should not be an issue because an empty list is as good as an error.
    * However throwing an exception may also be a good idea...
    */

  def descendingByRetweet: TweetList = Nil

  override def isEmpty: Boolean = true

  override def mostRetweetedAcc(maxRTAcc: Tweet): Tweet = maxRTAcc

  def filterAcc(p: Tweet => Boolean, acc: TweetSet): TweetSet = acc

  def union(that: TweetSet): TweetSet = that

  /**
    * The following methods are already implemented
    */

  def contains(tweet: Tweet): Boolean = false

  def incl(tweet: Tweet): TweetSet = new NonEmpty(tweet, new Empty, new Empty)

  def remove(tweet: Tweet): TweetSet = this

  def foreach(f: Tweet => Unit): Unit = ()
}

class NonEmpty(elem: Tweet, left: TweetSet, right: TweetSet) extends TweetSet {

  override def isEmpty: Boolean = false

  override def childrenAreEmpty: Boolean = left.isEmpty && right.isEmpty

  /**
    * descendingByRetweet: sorted list of tweets descending by the
    * number of retweets
    * @return
    */
  override def descendingByRetweet: TweetList = {
    new Cons(mostRetweeted, remove(mostRetweeted).descendingByRetweet)
  }

  /**
    * mostRetweeted: Tweet returns the tweet from this set which has the greatest retweet count.
    * This is the helper method that keeps track of the maximum Retweets
    *
    */
  override def mostRetweetedAcc(maxRTAcc: Tweet): Tweet = {
    if (left.isEmpty && right.isEmpty) elem
    else if (elem.retweets > maxRTAcc.retweets) filter(elem => elem.retweets > maxRTAcc.retweets).mostRetweetedAcc(elem)
    else if (left.isEmpty) right.filter(elem => elem.retweets > maxRTAcc.retweets).mostRetweetedAcc(elem)
    else if (right.isEmpty) left.filter(elem => elem.retweets > maxRTAcc.retweets).mostRetweetedAcc(elem)
    else maxRTAcc
  }

  /**
    * Helper method for the base class method filter:
    * def filter(p: Tweet => Boolean): TweetSet = filterAcc(p, new Empty)
    */
  def filterAcc(p: Tweet => Boolean, acc: TweetSet): TweetSet = {
    if (p(elem)) left.filterAcc(p, right.filterAcc(p, acc.incl(elem)))
    else left.filterAcc(p, right.filterAcc(p, acc))
  }

  /**
    * The following function is a recursive function
    * which is can be very inefficient when not done correctly.
    * Refer to the two commented out versions of the implementation - TODO: explain why
    */
  //TODO: explain why ^^
  def union(that: TweetSet): TweetSet =
    left.union(right.union(that.incl(elem)))

  //(left.union.(right.union.that)).incl(elem)
  //((left.union.right).union.other).incl elem

  /**
    * The following methods are already implemented
    */

  def contains(x: Tweet): Boolean =
    if (x.text < elem.text) left.contains(x)
    else if (elem.text < x.text) right.contains(x)
    //note: this means that we traverse the set until we find the tweet, then return a Boolean
    else true

  def incl(x: Tweet): TweetSet = {
    if (x.text < elem.text) new NonEmpty(elem, left.incl(x), right)
    else if (elem.text < x.text) new NonEmpty(elem, left, right.incl(x))
    //note: this means we traverse the set until the element has been included in a previous pass, to
    //the currently evaluated NonEmpty. Thus it was included in the previous pass, and only "rediscovered"
    //to be here, thus the returned NonEmpty is a new BST with NonEmpty.Tweet being the "one in the middle
    else this
  }

  def remove(tw: Tweet): TweetSet =
    if (tw.text < elem.text) new NonEmpty(elem, left.remove(tw), right)
    else if (elem.text < tw.text) new NonEmpty(elem, left, right.remove(tw))
    else left.union(right)

  def foreach(f: Tweet => Unit): Unit = {
    f(elem)
    left.foreach(f)
    right.foreach(f)
  }
}

trait TweetList {
  def head: Tweet

  def tail: TweetList

  def isEmpty: Boolean

  def foreach(f: Tweet => Unit): Unit =
    if (!isEmpty) {
      f(head)
      tail.foreach(f)
    }
}

object Nil extends TweetList {
  def head = throw new java.util.NoSuchElementException("head of EmptyList")

  def tail = throw new java.util.NoSuchElementException("tail of EmptyList")

  def isEmpty = true
}

class Cons(val head: Tweet, val tail: TweetList) extends TweetList {
  def isEmpty = false
}


object GoogleVsApple {
  val google = List("android", "Android", "galaxy", "Galaxy", "nexus", "Nexus")
  val apple = List("ios", "iOS", "iphone", "iPhone", "ipad", "iPad")

  lazy val appleTweets: TweetSet = new Empty //TweetReader.allTweets.filter(x => apple.exists(word => x.text.contains(word)) )
  lazy val googleTweets: TweetSet = TweetReader.allTweets.filter(x => google.exists(word => x.text.contains(word)) )

  /**
    * A list of all tweets mentioning a keyword from either apple or google,
    * sorted by the number of retweets.
    */
  lazy val trending: TweetList = (googleTweets union appleTweets).descendingByRetweet
}

object Main extends App {
  // Print the trending tweets
  GoogleVsApple.trending foreach println
}
