package objsets

import org.scalatest.FunSuite


import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class TweetSetSuite extends FunSuite {
  trait TestSets {
    val set1 = new Empty
    val set2 = set1.incl(new Tweet("a", "a body l o n g e s t", 20))
    val set3 = set2.incl(new Tweet("b", "b body a l m o s t", 20))
                 val c = new Tweet("c", "c body f o u r t h", 7)
                 val d = new Tweet("d", "d body third", 9)
    val set4c = set3.incl(c)
    val set4d = set3.incl(d)
    val set5 = set4c.incl(d)
    val e = new Tweet("e", "e body", 7)
    val f = new Tweet("f", "f body", 9)
    val g = new Tweet("g", "g body", 7)
    val h = new Tweet("h", "h body", 9)
    val i = new Tweet("i", "i body", 7)
    val j = new Tweet("j", "j body", 9)
    val k = new Tweet("k", "k body", 7)
    val l = new Tweet("l", "l body", 9)
    val m = new Tweet("m", "m body", 8)
    val setall = set1 incl e incl f incl g incl h incl i incl j incl k incl l incl m
  }

  def asSet(tweets: TweetSet): Set[Tweet] = {
    var res = Set[Tweet]()
    tweets.foreach(res += _)
    res
  }

  def size(set: TweetSet): Int = asSet(set).size

  test("filter: on empty set") {
    new TestSets {
      assert(size(set1.filter(tw => tw.user == "a")) === 0)
    }
  }

  test("removed a from set5") {
    new TestSets {
      //println("Filtered Set: " + asSet(set5.filter(tw => tw.user != "a")).toString())
      //println("")
      assert(size(set5.filter(tw => tw.user != "a")) === 3)
    }
  }

  test("filter: a on set5") {
    new TestSets {
      /*
      println("Original Set: " + asSet(set5).toString())
      println("Filtered Set: " + asSet(set5.filter(tw => tw.user == "a")).toString())
      println("")
      */
      assert(size(set5.filter(tw => tw.user == "a")) === 1)
    }
  }

  test("filter: 20 on set5") {
    new TestSets {
      assert(size(set5.filter(tw => tw.retweets == 20)) === 2)
    }
  }

  test("union: set4c and set4d") {
    new TestSets {
      val setu = set4c.union(set4d)
      assert(size(set4c.union(set4d)) === 4)
    }
  }

  test("union: with empty set (1)") {
    new TestSets {
      assert(size(set5.union(set1)) === 4)
    }
  }

  test("union: with empty set (2)") {
    new TestSets {
      assert(size(set1.union(set5)) === 4)
    }
  }

  test("union: major set") {
    new TestSets {
      //println("Big Set: " + asSet(set5.union(setall)).toString())
      assert(size(setall.union(set5)) == 13)
    }
  }

  test("maximum RT count") {
    new TestSets {
      assert(set5.mostRetweeted.retweets == 20, "Most RTs in this set was 20")
    }
  }

  test("descending: set5") {
    new TestSets {
      val trends = set5.descendingByRetweet
      assert(!trends.isEmpty)
      assert(trends.head.user == "a" || trends.head.user == "b")
    }
  }

  }
