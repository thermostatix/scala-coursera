package week4

abstract class Nat {

  //override def toString: String = "[" + this + "]"
  /**
    * Implementation of the abstract class Nat, without resorting to primitive types
    * Only a subset of methods for Integeres - only five methods
    */

  /**
    * test if number is zero
    *
    * @return
    */
  def isZero: Boolean

  /**
    * returns the predecessor if the number is >0, throw exception if number is zero
    *
    * @return
    */
  def predecessor: Nat

  /**
    * method for successor
    *
    * @return
    */
  def successor: Nat = new Succ(this)

  def +(that: Nat): Nat

  /**
    * method to implement subtraction
    * throw exception if this.-(that) < 0
    *
    * @param that
    * @return
    */
  def -(that: Nat): Nat

}


object Zero extends Nat {
  def isZero = true

  def predecessor = throw new java.lang.Exception("No predecessor available for Zero in set of Natural Numbers")

  def +(that: Nat) = that

  def -(that: Nat) = if(that.isZero) this
  else throw new java.lang.Exception("Cannot subtract anything from zero in the set of Natural Numbers")

}

class Succ(n: Nat) extends Nat {
  override def isZero = false

  override def predecessor: Nat = n

  def +(that: Nat) = new Succ(n + that)

  def -(that: Nat) = if (that.isZero) this else n - that.predecessor

 /**
  def +(that: Nat): Nat = {
    def loop(xAcc: Nat, increment: Nat): Nat = {
      if (increment.isZero) xAcc else loop(xAcc.successor, increment.predecessor)
    }

    loop(this, that)
  }

  override def -(that: Nat): Nat = {
    def loop(xAcc: Nat, decrement: Nat): Nat = {
      if (decrement.isZero) xAcc
      else if (xAcc.predecessor.isZero) throw new Exception("Decreasing beyond zero")
      else loop(xAcc.predecessor, decrement.predecessor)

    }

    loop(this, that)
  }
   */
}

