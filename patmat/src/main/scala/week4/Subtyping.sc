import week4._

object exprs {

  def show(e: Expr): String = e match {
    case Number(x) => x.toString
    case Sum(l, r) => show(l) + " + " + show(r)
  }

  show(Sum(Number(3), Number(1)))

  def eval(e: Expr): Int = e match {
    case Number(n) => n
    case Sum(e1, e2) => eval(e1) + eval(e2)
  }

  show(Number(eval(Sum(Number(1), Number(1)))))

}