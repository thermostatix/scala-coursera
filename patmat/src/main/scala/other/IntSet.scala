package other

//define the class
abstract class IntSet {

  def incl(x: Int): IntSet

  def contains(x: Int): Boolean

  def union(other: IntSet): IntSet

  def isEmpty: Boolean

  def foreach(f: Int => Unit): Unit

  //def max(x: Int)

  /**
    * This method takes a predicate and returns a subset of all the elements
    * in the original set for which the predicate is true.
    *
    * Question: Can we implement this method here, or should it remain abstract
    * and be implemented in the subclasses?
    *
    * Let's test it here
    */
  def filter(p: Int => Boolean): IntSet = filterAcc(p, new Empty)

  def filterAcc(p: Int => Boolean, acc: IntSet): IntSet

}

//subclass Empty of IntSet (IntSet is the superclass of Empty)
class Empty extends IntSet {

  def isEmpty: Boolean = true

  def foreach(f: Int => Unit): Unit = ()

  def contains(x: Int): Boolean = false

  def incl(x: Int): IntSet = new NonEmpty(x, new Empty, new Empty)

  override def toString: String = "."

  def union(other: IntSet): IntSet = other

  def filterAcc(p: Int => Boolean, acc: IntSet): IntSet = acc

}

//subclass NonEmpty of IntSet
class NonEmpty(elem: Int, left: IntSet, right: IntSet) extends IntSet {

  def isEmpty: Boolean = false

  def contains(x: Int): Boolean =
    if (x < elem) left contains x
    else if (x > elem) right contains x
    else true

  def incl(x: Int): IntSet =
    if (x < elem) new NonEmpty(elem, left incl x, right)
    else if (x > elem) new NonEmpty(elem, left, right incl x)
    else this

  override def toString: String = "{" + left + elem + right + "}"

  def union(other: IntSet): IntSet = {
    left.union(right.union(other.incl(elem)))
  }

  def filterAcc(p: Int => Boolean, acc: IntSet): IntSet = {
    if (p(elem)) left.filterAcc(p, right.filterAcc(p, acc.incl(elem)))
    else left.filterAcc(p, right.filterAcc(p, acc))
  }

  def foreach(f: Int => Unit): Unit = {
    f(elem)
    left.foreach(f)
    right.foreach(f)
  }

}