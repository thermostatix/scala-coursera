import patmat.Huffman.{_}

object play {

  times(string2Chars("dbdcaaabad"))

  combine(Nil) == List()

  val voilaCoded = List(1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1)
  val voilaDecode = List('v', 'o', 'i', 'l', 'a')

  val x = List()
  val y = 1 :: x
  (2 :: y).reverse

  val list = string2Chars("AAAAAAAABBBCDEFGH")
  val timesList = times(list)
  val testCodeTree = createCodeTree(list)

  val a: CodeTable = ('a', List(1, 1, 0, 1)) :: Nil
  val b: CodeTable = ('b', List(1, 1, 0, 0)) :: Nil
  val c = a ::: b

  val single: CodeTable = makeSingleNodeCodeTable('A', createCodeTree(list))

  /**
    * decode(frenchCode, voilaCoded)
    * decodedSecret
    * val testEncode = encode(frenchCode)(voilaDecode)
    */

}