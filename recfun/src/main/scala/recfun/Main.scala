package recfun

import math.Ordering

import java.lang
import java.lang.IllegalArgumentException

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()

    }
  }

  /**
    * Exercise 1
    */
  def pascal(c: Int, r: Int): Int = {
    if (r == c || c == 0) 1 else pascal(c - 1, r - 1) + pascal(c, r - 1)
  }

  /**
    * Exercise 2
    */
  def balance(chars: List[Char]): Boolean = {
    def checkAndAcc(acc: Int, head: Char, tail: List[Char]): Int = tail match {
      case Nil => acc + accIfBracket(head.toString)
      case _ => if (acc == 0 && head.toString == ")") -1 else checkAndAcc(acc + accIfBracket(head.toString), tail.head, tail.tail)
    }

    def accIfBracket(c: String): Int = c match {
      case "(" => 1
      case ")" => -1
      case _ => 0
    }

    if (checkAndAcc(0, chars.head, chars.tail) == 0) true else false

  }


  /**
    * Exercise 3
    */
  def countChange(money: Int, coins: List[Int]): Int = {
    def takeCoin(money: Int, coins: List[Int]): Int = coins match {
      case Nil => 0
      case coin :: Nil => if (money < 0) 0 else if (money == 0) 1 else takeCoin(money - coin, List(coin))
      case _ => {
        if (money < 0) 0 else if (money == 0) 1
        else takeCoin(money, coins.tail) + takeCoin(money - coins.head, coins)
      }
    }

    if (coins.isEmpty || money == 0) 0 else
      takeCoin(money, coins.sorted.reverse)
  }
}
